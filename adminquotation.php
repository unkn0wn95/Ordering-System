<?php
    session_start();
	require 'php/config.php';
//error_reporting(E_ALL ^ E_NOTICE);  to prevent from error coming out from php
// make sure user is logged in
if (!$_SESSION['username']) {
	echo '<script type="text/javascript">alert("You are not logged in.")</script>';
    $loginError = "You are not logged in.";
    include("index.php");
    exit();
}


?>

<?php

if(isset($_GET['order_details_id']))
{
    $order_details_id = $_GET['order_details_id'];
    // search in all table columns
    // using concat mysql function
    $query = "SELECT a.product_id AS product_id,a.product_name as product_name,a.product_image AS product_image,b.product_brand AS product_brand,b.quantity AS quantity,b.product_price AS product_price,b.order_id AS order_id,b.min_price AS min_price,b.max_price AS max_price FROM product a LEFT JOIN order_product b ON b.product_id = a.product_id WHERE b.order_details_id='$order_details_id'";
    $search_result = filterTable($query);
    	
}
// function to connect and execute the query
function filterTable($query)
{
	$connect = mysqli_connect("localhost", "root", "", "db_nesupply");
    $filter_Result = mysqli_query($connect, $query);
    return $filter_Result;
}

?>

<!DOCTYPE html>
<html>
 <head>
   <title>Ne-Supply</title>
   <link rel="stylesheet" type="text/css" href="style.css?v=5">
 </head>
 <body>
     <header>
        <div class="logo"><a href="#">Ne-<span>Supply</span></a></div>
     </header>
     <div id="container">
       <aside>
         <nav>
           <ul>
           <form action="php/logout.php" method="post">
        <li style="background-color:white;color:black;border:2px solid black;"><h4 style="text-align:center;">Welcome,<?php echo $_SESSION['username'] ?></h4><input id="logout" name="logout" type="submit" value="LOG OUT"/></li>
              </form>
        <li><a href="admin.php"><img src="image/order.png" width="20"height="20"> ORDER</a></li>
               <li><a href="adminproduct.php"><img src="image/cart.png" width="20"height="20"> PRODUCT</a></li>
        <li><a href="admincustomer.php"><img src="image/account.png" width="20"height="20"> CUSTOMER</a></li>
        <li><a href="adminlog.php"><img src="image/log.png" width="20" height="20"> LOG</a></li>
        <li><a href="adminreport.php"><img src="image/report.png" width="20" height="20"> REPORT</a></li>
           </ul>
          <nav>
       </aside>
       <section>
          <h1>QUOTATION</h1>
		  <article id="box">
		     <div class="box-top"><h2>PRODUCT</h2></article>
			 <div class="box-panel">
	       
			    <table id="product-list" >
				  <tr>
					 <th>Image</th>
					 <th>Product</th>
					 <th>Brand</th>					
           <th>Quantity</th>
           <th>Range Price</th>
           <th>Price</th>
				  </tr>
				 <?php
       
				 $total_price=0;
	     if($search_result)  {
		      if(mysqli_num_rows($search_result)){
                 while($row = mysqli_fetch_array($search_result))
                   {
             echo"
				  <tr>          
				    <input type='hidden' name='product_id' value='".$row['product_id']."'>
					<td><img src='".$row['product_image']."' width='150' height='100'></td>
					<td>".$row['product_name']."</td>
					<td>".$row['product_brand']."</td>
					<td>".$row['quantity']."</td>
          <td>RM ".$row['min_price']." - RM ".$row['max_price']." </td>

        <form action='adminquotation.php' method='post'>
          <input type='hidden' name='order_id[]' value='".$row['order_id']."'>
          <input type='hidden' name='product_brand[]' value='".$row['product_brand']."'>
					<td><input type='number' min='".$row['min_price']."' max='".$row['max_price']."'  name='product_price[]' value='".$row['product_price']."'></td>
                    
         
					
				 </tr>";
				 $total_price+=(($row['quantity'])*($row['product_price']));
                     }
                  echo "<tr>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td>Total Price</td>
                  <td>RM</td>
                  <input type='hidden' name='order_details_id' value='".$order_details_id."'>
                  <input type='hidden' name='total_price' value='".$total_price."'>
                  <td>".$total_price."</td>
                  </tr>";
                  }
				 else{
					 echo 'No Data ';
				 }
				 }
	         else{
					 echo 'Result Error';
			 }
       ?>
            
				</table>

        <br>
       
        <table border="0" width="100%">
         <tr >
          <th ><input type="submit" class="button darkblue" name="update" value="Update Quotation"></th>
          
          </tr>
        </table>
       </form>

			 </div>
         </div>
       </section>
     </div>
     <footer>
           <p>Copyright 2017&copy;NE-Supply</p>
      </footer>
 </body>
</html>
<?php
if(isset($_POST['update']))
     {
      $i=0;
      foreach($_POST['order_id'] as $val){
      $order_id=($_POST['order_id'][$i]);
      $product_brand=($_POST['product_brand'][$i]);
      $product_price=($_POST['product_price'][$i]);

      
      $query="UPDATE order_product SET product_price='$product_price' WHERE order_id='$order_id'";
      $query_run=mysqli_query($con,$query);

      $i++;
      }
      $order_details_id=($_POST['order_details_id']);
      $total_price=($_POST['total_price']);
      $updated="Waiting For Quotation Confirmation";

      $query="UPDATE order_details SET total_price='$total_price',status='$updated' WHERE order_details_id='$order_details_id'";

       $query_run=mysqli_query($con,$query); 

       $query="SELECT *FROM order_details WHERE order_details_id='$order_details_id'";
      $query_run=mysqli_query($con,$query);
      while($row = mysqli_fetch_array($query_run))
      {
        $cust_username=$row['cust_username'];
      }
       $date=date('Y-m-d'); 
       $time=date('h:i:sa');  
       $activity="admin updated quotation for ".$cust_username;
        $query="insert into log values('','$date','$time','$activity')";
        $query_run=mysqli_query($con,$query);      
            if($query_run)
          {
            echo'<script type="text/javascript"> alert("Quotation Updated");window.location.href = "admin.php";</script>';
          }
          else
          {
            echo'<script type="text/javascript">alert("Error")</script>';
          }
      }
?>