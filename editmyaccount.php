<?php
    session_start();
	require 'php/config.php';
//error_reporting(E_ALL ^ E_NOTICE);  to prevent from error coming out from php
// make sure user is logged in
if (!$_SESSION['username']) {
	echo '<script type="text/javascript">alert("You are not logged in.")</script>';
    $loginError = "You are not logged in.";
    include("index.php");
    exit();
}

	?>
<!DOCTYPE html>
<html>
 <head>
   <title>Ne-Supply</title>
   <link rel="stylesheet" type="text/css" href="style.css?v=5">
 </head>
 <body>
     <header>
        <div class="logo"><a href="#">Ne-<span>Supply</span></a></div>
     </header>
     <div id="container">
       <aside>
         <nav>
           <ul>
           <form action="php/logout.php" method="post">
			  <li style="background-color:white;color:black;border:2px solid black;"><h4 style="text-align:center;">Welcome,<?php echo $_SESSION['username'] ?></h4><input id="logout" name="logout" type="submit" value="LOG OUT"/></li>
              </form>
		      <li><a href="customer.php"><img src="image/dashboard.png" width="20"height="20"> HOME</a></li>
			  <li><a href="order.php"><img src="image/order.png" width="20"height="20"> ORDER</a></li>
               <li><a href="cart.php"><img src="image/cart.png" width="20"height="20"> CART</a></li>
			  <li><a href="myaccount.php"><img src="image/account.png" width="20"height="20"> MY ACCOUNT</a></li>
           </ul>
          <nav>
       </aside>
       <section>
	   <?php
           $query="select * from `user` where username='".$_SESSION['username']."'";

           $query_run =mysqli_query($con,$query);
	    if($query_run)  {
		      if(mysqli_num_rows($query_run)){
                 while($row = mysqli_fetch_array($query_run))
                   {
                     $username=$row["username"];
                     $password=$row["password"];
                     $email=$row["email"];
                     $phone=$row["phone"];
                     $cpname=$row["cpname"];
                     $regno=$row["regno"];
                     $address=$row["address"];

                     }
		         }
				 else{
					 echo 'No Data ';
				 }
				 }
	         else{
					 echo 'Result Error';
			 }
       ?>

          <h1>MY ACCOUNT</h1>
		  <article id="box">
		     <div class="box-top"><h2>Customer Details</h2></div>
			 <div class="box-panel">
			 <form action="editmyaccount.php" method="post">
			    <table id="myaccount">
				  <tr>
				    <td align="right">Email : </td>
					<td><input type="email" name="email" value="<?php echo $email ;?>"</td>

				  </tr>
				  <tr>
				    <td align="right">Password:</td>
					<td><input type="password" name="password" value="<?php echo $password ;?>"</td>

				  </tr>

				  <tr>
				    <td align="right">Phone Number :</td>
					<td><input type="phone" name="phone" value="<?php echo $phone; ?>"</td>

				  </tr>
				  <tr>
				    <td align="right">Company Name : </td>
					<td><input type="cpname" name="cpname" value="<?php echo $cpname;?>"</td>

				  </tr>
				   <tr>
				    <td align="right">Company Reg No : </td>
					<td><input type="regno" name="regno" value="<?php echo $regno ;?>" </td>

				  </tr>
				   <tr>
				    <td align="right">Company Address: </td>
					<td><textarea name="address" rows="5" cols="50" ><?php echo $address ;?></textarea>  </td>
				  </tr>
				   <tr>
				    <th colspan="2"><input type="submit" class="button darkblue" name="updateprofile" value="Update Profile"></th>
				  </tr>

				</table>
			  </form>
			 </div>
          </article>
       </section>
     </div>
     <footer>
           <p>Copyright 2017&copy;NE-Supply</p>
      </footer>
 </body>
</html>
<?php
	     if(isset($_POST['updateprofile']))
		 {
			$password=($_POST['password']);
			$email=($_POST['email']);
			$phone=($_POST['phone']);
			$cpname=($_POST['cpname']);
			$regno=($_POST['regno']);
			$address=($_POST['address']);

			$query="UPDATE user SET password='$password',email='$email',phone='$phone',cpname='$cpname',regno='$regno',address='$address' WHERE username='".$_SESSION['username']."'";

			 $query_run=mysqli_query($con,$query);
            if($query_run)
					{
						echo'<script type="text/javascript"> alert("Data Updated");window.location.href = "myaccount.php";</script>';
					}
					else
					{
						echo'<script type="text/javascript">alert("Error")</script>';
					}
		 }
?>
